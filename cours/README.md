# Mémos

* Mémos
  * [Commandes et fichiers essentiels du système](./basics.md)
  * [Partitionnement, LVM](./part.md)
  * [Réseau sous CentOS 7](./centos_network.md)
* [Intro à la crypto](./crypto.md)
* [systemd](./systemd.md)
* [FHS](./fhs.md)
