# TP1 : Configuration système

Dans ce TP, on va passer en revue des éléments de configurations élémentaires du système.

Vous pouvez effectuer ces actions dans la première VM. On la clonera ensuite avec toutes les configurations pré-effectuées.

Au menu :
* gestion d'utilisateurs
  * sudo
  * SSH et clés
* configuration réseau
* gestion de partitions
* gestion de services

# Sommaire

<!-- vim-markdown-toc GitLab -->

* [0. Préliminaires](#0-préliminaires)
* [I. Utilisateurs](#i-utilisateurs)
    * [1. Création et configuration](#1-création-et-configuration)
    * [2. SSH](#2-ssh)
* [II. Configuration réseau](#ii-configuration-réseau)
    * [1. Nom de domaine](#1-nom-de-domaine)
    * [2. Serveur DNS](#2-serveur-dns)
* [III. Partitionnement](#iii-partitionnement)
    * [1. Préparation de la VM](#1-préparation-de-la-vm)
    * [2. Partitionnement](#2-partitionnement)
* [IV. Gestion de services](#iv-gestion-de-services)
    * [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
    * [2. Création de service](#2-création-de-service)
        * [A. Unité simpliste](#a-unité-simpliste)
        * [B. Modification de l'unité](#b-modification-de-lunité)

<!-- vim-markdown-toc -->

# 0. Préliminaires

Installer `vim` sur la machine afin de faciliter l'édition des fichiers.

Désactiver SELinux :
```bash
$ sudo setenforce 0

$ sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
```

# I. Utilisateurs

## 1. Création et configuration

Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :
* le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans `/home`
* le shell de l'utilisateur est `/bin/bash`

Créer un nouveau groupe `admins` qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.

Pour permettre à ce groupe d'accéder aux droits `root` :
* il faut modifier le fichier `/etc/sudoers`
* on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur
* la commande `visudo` permet d'éditer le fichier, avec un check de syntaxe avant fermeture
* ajouter une ligne basique qui permet au groupe d'avoir tous les droits (inspirez vous de la ligne avec le groupe `wheel`)

Ajouter votre utilisateur à ce groupe `admins`.

---

1. Utilisateur créé et configuré
2. Groupe `admins` créé
3. Groupe `admins` ajouté au fichier `/etc/sudoers`
4. Ajout de l'utilisateur au groupe `admins`

## 2. SSH

Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine. 

Pour cela :
* il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )
  * génération de clé depuis VOTRE poste donc
  * sur Windows, on peut le faire avec le programme `puttygen.exe` qui est livré avec `putty.exe`
* déposer la clé dans le fichier `/home/<USER>/.ssh/authorized_keys` de la machine que l'on souhaite administrer
  * on peut le faire à la main
  * ou avec la commande `ssh-copy-id`

Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.

# II. Configuration réseau

## 1. Nom de domaine

Définir un nom de domaine à la machine

## 2. Serveur DNS

Définir l'utilisation de `1.1.1.1` (ou autre adresse personnalisée) comme serveur DNS de la machine.

# III. Partitionnement

## 1. Préparation de la VM

Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

## 2. Partitionnement

Utilisez LVM pour :
* agréger les deux disques en un seul *volume group*
* créer 3 *logical volumes* de 1 Go chacun
* formater ces partitions en `ext4`
* monter ces partitions pour qu'elles soient accessibles aux points de montage `/mnt/part1`, `/mnt/part2` et `/mnt/part3`.

Grâce au fichier `/etc/fstab`, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

# IV. Gestion de services

Au sein des systèmes GNU/Linux les plus utilisés, c'est *systemd* qui est utilisé comme gestionnaire de services (entre autres).

Pour manipuler les services entretenus par *systemd*, on utilise la commande `systemctl`.

On peut lister les unités `systemd` actives de la machine `systemctl list-units -t service`.

> Beaucoup de commandes `systemd` qui écrivent des choses en sortie standard sont automatiquement munie d'un pager (pipé dans `less`). On peut ajouter l'option `--no-pager` pour se débarasser de ce comportement

Pour obtenir plus de détails sur une unitée donnée
* `systemctl is-active <UNIT>`
  * détermine si l'unité est actuellement en cours de fonctionnement
* `systemctl is-enabled <UNIT>`
  * détermine si l'unité est activée ("activée" = démarre au boot)
* `systemctl status <UNIT>`
  * affiche l'état complet d'une unité donnée
  * comme le path où elle est définie, si elle a été enable, tous les processus liés, etc.
* `systemctl cat <UNIT>`
  * affiche les fichiers qui définissent l'unité

## 1. Interaction avec un service existant

Parmi les services système déjà installés sur CentOS, il existe `firewalld`. Cet utilitaire est l'outil de firewalling de CentOS.

Assurez-vous que :
* l'unité est démarrée
* l'unitée est activée (elle se lance automatiquement au démarrage)

## 2. Création de service

### A. Unité simpliste

Créer un fichier qui définit une unité de service `web.service` dans le répertoire `/etc/systemd/system`.

Déposer le contenu suivant :
```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888

[Install]
WantedBy=multi-user.target
```

Le but de cette unité est de lancer un serveur web sur le port 8888 de la machine. **N'oubliez pas d'[ouvrir ce port](../../cours/centos_network.md#interagir-avec-le-firewall).**

Une fois l'unité de service créée, il faut demander à *systemd* de relire les fichiers de configuration :
```bash
$ sudo systemctl daemon-reload
```

Enfin, on peut interagir avec notre unité :
```bash
$ sudo systemctl status web
$ sudo systemctl start web
$ sudo systemctl enable web
```

Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande `curl` sur l'IP de la VM, port 8888.

### B. Modification de l'unité

Créer un utilisateur `web`.

Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses :
* `User=` afin de lancer le serveur avec l'utilisateur `web` dédié
* `WorkingDirectory=` afin de lancer le serveur depuis un dossier spécifique, choisissez un dossier que vous avez créé dans `/srv`
* ces deux clauses sont à positionner dans la section `[Service]` de votre unité

Placer un fichier de votre choix dans le dossier créé dans `/srv` et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur `web`.
